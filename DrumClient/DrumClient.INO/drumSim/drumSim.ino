int myDelay = 250;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(250000);
}

void loop() {
  // put your main code here, to run repeatedly:
  Serial.println("bass");
  delay(myDelay);
  Serial.println("snare");
  delay(myDelay);
  Serial.println("bass");
  delay(myDelay);
  Serial.println("snare");
  delay(myDelay);
  Serial.println("bass");
  delay(myDelay);
  Serial.println("snare");
  delay(myDelay);
  Serial.println("hihat");
  delay(myDelay);
}
