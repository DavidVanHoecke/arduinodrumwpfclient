﻿using System.IO.Ports;
using System.Linq;
using System.Media;
using System.Windows;
using ArduinoLibrary;

namespace DrumClient.WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        readonly ArduinoDeviceManager arduinoDeviceManager = new ArduinoLibrary.ArduinoDeviceManager();
        readonly SoundPlayer player = new SoundPlayer();

        public MainWindow()
        {
            InitializeComponent();

            ListPorts();

            OpenPorts();
        }

        private void ListPorts()
        {
            listBoxPorts.ItemsSource = arduinoDeviceManager.SerialPorts.Keys;
        }

        private void OpenPorts()
        {
            arduinoDeviceManager.SerialPorts.Values.ToList().ForEach(port =>
            {
                port.BaudRate = 250000;
                port.Open();
                port.DataReceived += Port_DataReceived;
            });
        }

        private void Port_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            string message = ((SerialPort)sender).ReadLine().Replace("\r", "");
            
            switch (message)
            {
                case "bass":
                    player.SoundLocation = @"resources\Wime Snare Drum.wav";
                    break;

                case "snare":
                    player.SoundLocation = @"resources\909bas.wav";
                    break;

                case "hihat":
                    player.SoundLocation = @"resources\LONGHAT.wav";
                    break;
            }

            player.Load();
            player.Play();
        }
    }
}
